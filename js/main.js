/*
Bài 1: tìm số nguyên dương nhỏ nhất
1 + 2 + ... + n > 10000
*/

var sum = 0;

for (var number = 1; sum < 10000; number++) {
    sum += number;
}
document.getElementById("minNumber").innerHTML = number;

/*
Bài 2: viết chương trình nhập vào 2 số: x, n
Tính tổng: S(n) = x + x^2 + x^3 + ... + x^n
*/

document.querySelector('#btnCalSum').onclick = function () {
    var inputNumberX = +document.querySelector('#numberX').value;
    var inputNumberN = +document.querySelector('#numberN').value;
    var sum2 = inputNumberX;

    for (var number2 = 2; number2 <= inputNumberN; number2++) {
        sum2 += Math.pow(inputNumberX, number2);
    }

    document.querySelector('#result2').innerHTML = 'x = ' + inputNumberX + '<br>' + 'n = ' + inputNumberN + '<br>' + 'Kết quả là: ' + sum2;

    document.querySelector('#numberX').value = '';
    document.querySelector('#numberN').value = '';
}

/*
Bài 3: nhập vào n. Tính giai thừa: 1*2*...*n
*/

document.querySelector('#btnCal3').onclick = function () {
    var inputNumber3 = +document.querySelector('#userInput3').value;
    var result3 = 1;

    if (inputNumber3 < 1) {
        result3 = 0;
    } else {
        for (var number3 = 1; number3 <= inputNumber3; number3++) {
            result3 *= number3;
        }
    }

    document.querySelector('#result3').innerHTML = 'n = ' + inputNumber3 + '<br />' + 'Kết quả là: ' + result3;

    document.querySelector('#userInput3').value = '';
}

/*
Bài 4: Viết chương trình khi click vào button sẽ tạo ra 10 thẻ div
Div ở vị trí chẵn có background màu đỏ
Div ở vị trí lẻ có background màu xanh
*/

document.querySelector('#btnHomework4').onclick = function () {
    var divQty = 10;
    var divStatus = '';
    var divCreate = '';
    var divResult = document.querySelector('#resultHomework4');
    var divClass = '';

    for (var number4 = 1; number4 <= divQty; number4++) {
        if (number4 % 2 == 0) {
            divStatus = 'chẵn';
            divClass = 'text-light bg-danger p-2 m-2';
        } else {
            divStatus = 'lẻ';
            divClass = 'text-light bg-primary p-2 m-2';
        }
        divCreate = document.createElement('div');
        divCreate.innerHTML = 'Div ' + divStatus + ' ' + number4;
        divCreate.className = divClass;
        divResult.appendChild(divCreate);
    }
}